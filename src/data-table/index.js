// @flow

import firebase from 'firebase';

type Data = Array<{ [key: string]: any }>;

declare type SyntheticMouseEventElement<E> = {
    target: E
} & SyntheticMouseEvent<EventTarget>;

export default class DataTable {
    #parent: HTMLElement;
    #data: Data;
    tableTitle: string;
    #rows = HTMLElement;
    #pageLength = 5;
    #pagingRange = 5;
    #delta;
    #activePage = 0;
    #pages: NodeList<HTMLElement>;
    #isAsc = false;
    #sortField = 0;

    constructor(parent: HTMLElement, title: string) {
        this.#parent = parent;
        this.tableTitle = title;
    }

    normalizeData(data: Data): Data {
        let template = data.reduce((a, c) => (Object.keys(c).forEach(key => a[key] = ''), a), {});
        return data.map(obj => {
            if (Object.keys(obj).length < Object.keys(template).length) obj = {...template, ...obj};
            let newObj = {};
            for (let key in obj) {
                if (obj[key] instanceof Date) newObj[key] = obj[key].getFullYear() + '-' + (obj[key].getMonth() + 1) + '-' + obj[key].getDate();
                else if (obj[key] === undefined) newObj[key] = '';
                else newObj[key] = obj[key];
            }
            return newObj;
        })
    }

    build(data: Data) {
        this.#parent.innerHTML = '';
        this.#data = this.normalizeData(data);
        let table = document.createElement('table');
        let tr = document.createElement('tr');

        table.appendChild(tr);
        tr.classList.add('table__row', 'header-row');
        for (let key in data[0]) {
            let th = document.createElement('th');
            th.textContent = key;
            tr.appendChild(th);
        }

        this.#data.forEach(obj => {
            tr = table.insertRow(-1);
            tr.classList.add('table__row', 'data-row');
            for (let key in obj) {
                if (obj.hasOwnProperty(key)) {
                    let td = tr.insertCell(-1);
                    let data = document.createTextNode(obj[key]);
                    td.appendChild(data);
                }
            }
        });
        table.classList.add('data-table', this.tableTitle);
        this.#parent.appendChild(table);
        this.#rows = table.querySelectorAll('.data-row');
        this.buildPagination();
        this.buildPageSize();
        this.sorting();
    }

    buildPagination() {
        let ul = document.createElement('ul');
        let div = document.createElement('div');

        if (this.table) this.#parent.insertBefore(div, this.table.nextSibling);
        div.classList.add('pagination-wrapper', `${this.tableTitle}-pagination-wrapper`);
        div.appendChild(ul);
        ul.classList.add('pagination-list', `${this.tableTitle}-pagination-list`, 'pagination-list_more');
        for (let i = 1; i <= this.#pagingRange; i++) {
            let li = document.createElement('li');
            let a = document.createElement('a');

            if (i === this.#pagingRange) {
                a.textContent = this.numPages.toString();
            } else a.textContent = i.toString();
            a.href = '#';
            a.classList.add('item__page');
            li.appendChild(a);
            li.classList.add('pagination-list__item', 'item');
            ul.appendChild(li);
        }
        this.paging();
        if (this.paginationList) this.paginationList.onclick = (e: SyntheticMouseEventElement<HTMLElement>) => {
            e.preventDefault();
            if (e.target.className === 'item__page') {
                firebase.database().ref('/options/activePage').set(+e.target.textContent - 1);
                this.paging();
            }

        }
    }

    paging() {
        let start;
        this.#delta = this.#pagingRange - 2;
        if (this.paginationList) this.#pages = this.paginationList.querySelectorAll('.item__page');

        if (this.#delta > this.numPages) this.#delta = this.numPages;
        start = Math.min(Math.max((this.#activePage - Math.floor(this.#delta / 2)), 1), this.numPages - this.#delta);
        if (this.paginationList) {
            if (this.#activePage >= this.numPages - this.#delta) {
                this.paginationList.classList.remove('pagination-list_more');
                start = this.numPages - this.#delta - 1;
            } else this.paginationList.classList.add('pagination-list_more');

            if (this.#activePage >= this.#delta) this.paginationList.classList.add('pagination-list_less');
            else this.paginationList.classList.remove('pagination-list_less');
        }

        for (let i = 1; i <= this.#delta; i++) {
            this.#pages[i].textContent = (start + i).toString();
        }

        for (let i = 0; i < this.#rows.length; i++) {
            this.#rows[i].style.display = 'none';
        }
        if (this.tableDataRows) {
            for (let i = this.#pageLength * this.#activePage; i < this.#pageLength * this.#activePage + this.#pageLength; i++) {
                if (this.tableDataRows[i] !== undefined) this.tableDataRows[i].style.display = 'table-row';
            }
        }
    }

    buildPageSize() {
        for (let i = this.#pageLength; i < this.#rows.length; i++) {
            this.#rows[i].style.display = 'none';
        }
        let sel = document.createElement('select');
        for (let i = 1; i <= 5; i++) {
            sel.add(document.createElement('option'));
            sel.options[i - 1].value = (5 * i ** 2).toString();
            sel.options[i - 1].text = `${5 * i ** 2} rows`;
            if (5 * i ** 2 === this.#pageLength) sel.options[i - 1].selected = true;
        }

        if (this.paginationWrapper) this.paginationWrapper.appendChild(sel);
        if (this.lengthSelect) this.lengthSelect.onchange = (e) => {
            firebase.database().ref('/options/pageLength').set(+e.currentTarget.options[e.currentTarget.selectedIndex].value);
            firebase.database().ref('/options/activePage').set(0);
            this.pageSize();
        }
    }

    pageSize() {
        if (this.paginationList) this.#pages = this.paginationList.querySelectorAll('.item__page');
        if (this.tableDataRows) this.#rows = this.tableDataRows;

        for (let i = 0; i < this.#rows.length; i++) {
            this.#rows[i].style.display = 'none';
        }
        for (let i = 0; i < this.#pageLength; i++) {
            this.#rows[i].style.display = 'table-row';
        }
        for (let i = 0; i < this.#pagingRange; i++) {
            if (i === this.#pagingRange - 1) {
                this.#pages[i].textContent = this.numPages.toString();
            } else this.#pages[i].textContent = (i + 1).toString();
        }
        if (this.paginationList) this.paginationList.classList.remove('pagination-list_less');
        if (this.paginationList) this.paginationList.classList.add('pagination-list_more');
    }

    sorting() {
        if (this.tableHeader) {
            let titles: Array<HTMLElement> = Array.from(this.tableHeader);
            this.sortLogic();
            titles.forEach((th: HTMLElement) => {
                th.onclick = () => {
                    firebase.database().ref('/options/sortField').set(titles.indexOf(th));
                    firebase.database().ref('/options/isAsc').set(!this.#isAsc);
                    this.sortLogic();
                }
            })
        }
    }

    sortLogic() {
        if (this.tableDataRows) {
            Array.from(this.tableDataRows)
                .sort(this.compare(this.#sortField, this.#isAsc))
                .forEach((tr) => {
                    tr.style.display = 'none';
                    this.table ? this.table.appendChild(tr) : undefined;
                });

            if (this.tableDataRows) {
                for (let i = this.#pageLength * this.#activePage; i < this.#pageLength * this.#activePage + this.#pageLength; i++) {
                    if (this.tableDataRows[i] !== undefined) this.tableDataRows[i].style.display = 'table-row';
                }
            }
        }
    }

    static cellValue(tr: HTMLElement, i: number) {
        return tr.children[i].textContent !== undefined ? tr.children[i].textContent : '';
    }

    compare(i: number, compareOpt: boolean) {
        return (a: HTMLElement, b: HTMLElement) => {
            return ((val1: string, val2: string) => {
                return val1 !== '' && val2 !== '' && !isNaN(val1) && !isNaN(val2) ? +val1 - +val2 : val1.toString().localeCompare(val2);
            })(DataTable.cellValue(compareOpt ? a : b, i), DataTable.cellValue(compareOpt ? b : a, i));
        }
    }

    get numPages(): number {
        return Math.ceil(this.#rows.length / this.#pageLength);
    }

    get paginationWrapper() {
        return this.#parent.querySelector(`.${this.tableTitle}-pagination-wrapper`) ? document.querySelector(`.${this.tableTitle}-pagination-wrapper`) : undefined;
    }

    get paginationList() {
        return this.paginationWrapper ? this.paginationWrapper.querySelector(`.${this.tableTitle}-pagination-list`) : undefined;
    }

    get lengthSelect() {
        return this.paginationWrapper ? this.paginationWrapper.querySelector('select') : undefined;
    }

    get table() {
        return this.#parent.querySelector(`.${this.tableTitle}`) ? document.querySelector(`.${this.tableTitle}`) : undefined;
    }

    get headerRow() {
        return this.table ? this.table.querySelector('.header-row') : undefined;
    }

    get tableHeader() {
        return this.headerRow ? this.headerRow.querySelectorAll('th') : undefined;
    }

    get tableDataRows() {
        return this.table ? this.table.querySelectorAll('.data-row') : undefined;
    }

    set pageLength(val: number) {
        this.#pageLength = val;
    }

    set activePage(val: number) {
        this.#activePage = val;
    }

    set isAsc(val: boolean) {
        this.#isAsc = val;
    }

    set sortField(val: number) {
        this.#sortField = val;
    }

};
