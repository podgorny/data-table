// @flow

import firebase from 'firebase';

import './styles.styl';

import DataTable from './data-table';

const config = {
    apiKey: "AIzaSyD4azVNk9xFL7Vz0V3BFtzc5VAlAI_-GWk",
    authDomain: "datatable-879e3.firebaseapp.com",
    databaseURL: "https://datatable-879e3.firebaseio.com",
    projectId: "datatable-879e3",
    storageBucket: "datatable-879e3.appspot.com",
    messagingSenderId: "634714822459"
};
firebase.initializeApp(config);
const database = firebase.database().ref();

if (document.body) {
    const table = new DataTable(document.body, 'data-table-laureates');

    database.child('laureates').once('value').then(snapshot => {
        let data = snapshot.val().map(({id, firstname, surname, born, died, bornCountry, bornCity, diedCountry, diedCity, gender, prizes}) => {
            let res = {
                '#': Number(id),
                'First Name': firstname,
                'Last Name': surname,
                'Born Location': [bornCity, bornCountry].filter(i => i).join(', '),
                'Died Location': [diedCity, diedCountry].filter(i => i).join(', '),
                'Gender': gender,
                'Prizes': prizes ? prizes
                    .filter(({year, category}) => year && category)
                    .map(({year, category}) => `${category}, ${year}`).join('; ') : ''
            };

            if (born !== '0000-00-00') {
                res = {
                    ...res,
                    Born: new Date(Date.parse(born.replace('-00-00', '-01-01')))
                };
            } else {
            }

            if (died !== '0000-00-00') {
                res = {
                    ...res,
                    Died: new Date(Date.parse(died.replace('-00-00', '-01-01')))
                };
            }

            return res;
        });
        // table.isAsc = true;
        // table.sortField = 0;
        table.build(data);

        database.child('options').on('value', snapshot => {
            table.activePage = snapshot.val().activePage;
            table.pageLength = snapshot.val().pageLength;
            table.sortField = snapshot.val().sortField;
            table.isAsc = snapshot.val().isAsc;
            table.build(data);
        });
    });
}
