module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    browsers: [
                        '> 2%',
                        'ie 11',
                        'safari > 9'
                    ]
                }
            }
        ]
    ],
    plugins: [
        '@babel/transform-flow-strip-types',
        '@babel/plugin-proposal-private-methods',
        '@babel/plugin-proposal-class-properties'
    ]
};